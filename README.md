# Broker Monitoring

Um site para monitar Criptomoedas e Exchanges

## Instruções para rodar

1. Instale as dependências:
```
npm install
```

2. Para executar no ambiente de desenvolvimento:
```
npm start
```

3. Para buildar:
```
npm build
```
