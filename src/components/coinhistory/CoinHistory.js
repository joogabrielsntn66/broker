import React, { useEffect, useState } from "react";
import { Card, Col, Form, Row } from "react-bootstrap";
import { Line } from "react-chartjs-2";
import CoinsProvider from "../../services/coins";
import { FcLineChart } from "react-icons/fc";

const CoinHistory = (props) => {
  const coins = props.coins;

  const [coinHistory, setCoinHistory] = useState({});
  const [crypto, setCrypto] = useState('bitcoin');
  const dateInterval = 'd1';

  const changeCrypto = (newCrypto) => {
    setCrypto(newCrypto);
  };

  const refreshCoinHistory = () => {
    CoinsProvider.getCoinHistory(crypto, dateInterval).then(data => {
      setCoinHistory(data);
    });
  };

  useEffect(() => {
    refreshCoinHistory();
  }, [crypto, dateInterval, setCoinHistory]);

  return (
    <Card>
      <Card.Header>
        <Card.Title>
          <FcLineChart size={42} /> Cryptocurrency History
        </Card.Title>
      </Card.Header>
      <Card.Body>
        <Row>
          <Col md={12}>
            <Form>
              <Form.Group>
                <Form.Label>Select a coin: </Form.Label>
                <Form.Control as="select" onChange={e => {
                  changeCrypto(e.target.value);
                }}>
                  {coins.map((coin) =>
                    <option key={coin.id} value={coin.id}>{coin.name}</option>
                  )}
                </Form.Control>
              </Form.Group>
            </Form>
          </Col>
        </Row>
        <Row>
          <Col md={12}>
            <Line
              data={{
                labels: coinHistory.dates,
                datasets: [
                  {
                    label: coinHistory.id,
                    data: coinHistory.prices,
                    fill: true,
                    backgroundColor: 'rgb(128, 189, 255)',
                    borderColor: 'rgba(0, 123, 255, 1)',
                  },
                ]
              }}
            />
          </Col>
        </Row>
      </Card.Body>
    </Card>
  );
};

export default CoinHistory;