import React from "react";
import { Button, Card, Col, Row, Table } from "react-bootstrap";
import { FcCurrencyExchange } from "react-icons/fc";

const TopExchanges = (props) => {
  const exchanges = props.exchanges;

  return (
    <Card>
      <Card.Header>
        <Card.Title><FcCurrencyExchange size={42} /> Top Exchanges</Card.Title>
      </Card.Header>
      <Table striped bordered hover responsive>
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Volume $</th>
          </tr>
        </thead>
        <tbody>
          {exchanges.map((exchange) =>
            <tr key={exchange.exchangeId}>
              <td>{exchange.rank}</td>
              <td>{exchange.name}</td>
              <td>$ {parseFloat(exchange.volumeUsd).toFixed(4)}</td>
            </tr>
          )}
        </tbody>
      </Table>
      <Card.Body>
        <Row className="justify-content-end">
          <Col md="auto">
            <Button variant="primary" href="/exchanges">See More</Button>
          </Col>
        </Row>
      </Card.Body>
    </Card>
  );
};

export default TopExchanges;