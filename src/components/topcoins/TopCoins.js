import React from "react";
import { Button, Card, Col, Row, Table } from "react-bootstrap";
import { FcCurrencyExchange } from "react-icons/fc";
import ChangePrice from "../coinprice/ChangePrice";

const Topcoins = (props) => {
  const coins = props.coins;

  return (
    <Card>
      <Card.Header>
        <Card.Title><FcCurrencyExchange size={42} /> Top Cryptocurrencies</Card.Title>
      </Card.Header>
      <Table striped bordered hover responsive>
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Volume $</th>
            <th>Change</th>
          </tr>
        </thead>
        <tbody>
          {coins.map((coin) =>
            <tr key={coin.id}>
              <td>{coin.rank}</td>
              <td>{coin.name}</td>
              <td>$ {parseFloat(coin.priceUsd).toFixed(4)}</td>
              <td>
                <ChangePrice changePercent={coin.changePercent24Hr} />
              </td>
            </tr>
          )}
        </tbody>
      </Table>
      <Card.Body>
        <Row className="justify-content-end">
          <Col md="auto">
            <Button variant="primary" href="/coins">See More</Button>
          </Col>
        </Row>
      </Card.Body>
    </Card>
  );
};

export default Topcoins;