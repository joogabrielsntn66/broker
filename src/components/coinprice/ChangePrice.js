import React from "react";
import { Badge } from "react-bootstrap";

const ChangePrice = (props) => {
  const changePercent = parseFloat(props.changePercent).toFixed(4) || 0.0;

  if (changePercent < 0) {
    return (
      <h5>
        <Badge variant="danger">
          {changePercent}%
          </Badge>
      </h5>
    );
  }

  return (
    <h5>
      <Badge variant="success">
        +{changePercent}%
        </Badge>
    </h5>
  );
}

export default ChangePrice;