import React, { useEffect, useState } from "react";
import { Card, Col, Row } from "react-bootstrap";
import { RiCoinsFill } from "react-icons/ri";
import CoinsProvider from "../../services/coins";
import { SECONDS_TO_REFRESH } from "../../utils/constants";
import ChangePrice from "./ChangePrice";

const CoinPrice = (props) => {
  const { color, coinName, coinId } = props;
  const [coinData, setCoinData] = useState({});

  const fetchCoinData = () => {
    CoinsProvider.getCoinById(coinId).then(data => {
      setCoinData(data);
    });
  };

  useEffect(() => {
    fetchCoinData();

    const interval = setInterval(() => {
      fetchCoinData();
    }, SECONDS_TO_REFRESH * 1000);

    return () => clearInterval(interval);
  }, []);

  return (
    <>
      <Card bg={color} text="white">
        <Card.Body>
          <Row className="justify-content-between">
            <Col>
              <Card.Title><RiCoinsFill /> {coinName}</Card.Title>
            </Col>
            <Col>
              Gráfico
            </Col>
          </Row>
          <Row className="justify-content-between">
            <Col>
              <ChangePrice changePercent={coinData.changePercent24Hr} />
            </Col>
            <Col>
              <h4>$ {parseFloat(coinData.priceUsd).toFixed(4)}</h4>
            </Col>
          </Row>
        </Card.Body>
      </Card>
    </>
  );
};

export default CoinPrice;