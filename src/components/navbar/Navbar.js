import React from "react";
import { Container, Nav, Navbar } from "react-bootstrap";
import { FcHome, FcCurrencyExchange } from "react-icons/fc";

import "./Navbar.scss";

const BrokerNav = () => {
  return (
    <>
      <Navbar collapseOnSelect bg="white" variant="light" expand="lg">
        <Container>
          <Navbar.Brand href="/">Broker</Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav>
              <Nav.Link href="/" className="pl-2">
                <FcHome /> Home
              </Nav.Link>
              <Nav.Link href="/coins" className="pl-2">
                <FcCurrencyExchange /> Coins
              </Nav.Link>
              <Nav.Link href="/exchanges" className="pl-2">
              <FcCurrencyExchange /> Exchanges
              </Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </>
  );
};

export default BrokerNav;