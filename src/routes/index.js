import React from "react";
import { Switch, Route } from "react-router-dom";

import Home from "../pages/home/Home";
import Coins from "../pages/coins/Coins";
import Exchanges from "../pages/exchanges/Exchanges";

const Routes = () => {
  return (
    <Switch>
      <Route path="/" exact component={Home} />
      <Route path="/coins" component={Coins} />
      <Route path="/exchanges" component={Exchanges} />
    </Switch>
  );
};

export default Routes;