import React from "react";
import './App.scss';

import Navbar from "./components/navbar/Navbar";

const App = (props) => {
  return (
    <div className="bg-light">
      <Navbar></Navbar>
      {props.children}
    </div>
  );
}

export default App;