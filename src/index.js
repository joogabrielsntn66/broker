import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { Router, Route, Switch } from 'react-router-dom';
import history from './services/history';

import Home from "./pages/home/Home";
import Coins from "./pages/coins/Coins";
import Exchanges from "./pages/exchanges/Exchanges";

ReactDOM.render(
  <React.StrictMode>
    <Router history={history}>
      <App>
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/coins" component={Coins} />
          <Route path="/exchanges" component={Exchanges} />
        </Switch>
      </App>
    </Router>
  </React.StrictMode>,
  document.getElementById('root')
);
