import React, { useEffect, useMemo, useState } from "react";
import { Button, Col, Container, Row } from "react-bootstrap";
import BrokerTable from "../../components/table/Table";
import CoinsProvider from "../../services/coins";
import ChangePrice from "../../components/coinprice/ChangePrice";

const Coins = () => {
  const [coins, setCoins] = useState([]);

  useEffect(() => {
    CoinsProvider.getAllCoins().then(data => {
      setCoins(data);
    });
  }, []);

  const columns = useMemo(() => [
    {
      Header: "#",
      accessor: "rank",
    },
    {
      Header: "Name",
      accessor: "name",
    },
    {
      Header: "Symbol",
      accessor: "symbol",
    },
    {
      Header: "Price($)",
      accessor: "priceUsd",
      Cell: ({ cell: { value } }) => {
        return (
          <>
            $ {parseFloat(value).toFixed(4).toString()}
          </>
        );
      },
    },
    {
      Header: "Supply",
      accessor: "supply",
      Cell: ({ cell: { value } }) => {
        return (
          <>
            $ {parseFloat(value).toFixed(4).toString()}
          </>
        );
      },
    },
    {
      Header: "Change 24hrs",
      accessor: "changePercent24Hr",
      Cell: ({ cell: { value } }) => {
        return <ChangePrice changePercent={value} />;
      },
    },
    {
      Header: "Explorer",
      accessor: "explorer",
      Cell: ({ cell: { value } }) => {
        return (
          <Button variant="primary" href={value}>
            Explorer
          </Button>
        );
      },
    },
  ], []);

  return (
    <Container fluid>
      <Row className="pt-4 pb-3">
        <Col xs={12} md={12} sm={12}>
          <BrokerTable
            columns={columns}
            data={coins}
            title="Coins"
          />
        </Col>
      </Row>
    </Container>
  );
};

export default Coins;