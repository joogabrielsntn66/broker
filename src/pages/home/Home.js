import { Col, Row, Container } from "react-bootstrap";
import React, { useEffect, useState } from "react";

import CoinsProvider from "../../services/coins";
import TopCoins from "../../components/topcoins/TopCoins";
import TopExchanges from "../../components/topexchanges/TopExchanges";
import CoinHistory from "../../components/coinhistory/CoinHistory";
import CoinPrice from "../../components/coinprice/CoinPrice";
import { SECONDS_TO_REFRESH } from "../../utils/constants";

const Home = () => {
  const [coins, setCoins] = useState([]);
  const [exchanges, setExchanges] = useState([]);

  /**
   * Fetch three top coins
   * 
   * @returns void
   */
  const fetchCoins = async () => {
    let coins = await CoinsProvider.getTopCoins();
    setCoins(coins);
  };

  /**
   * Fetch three top Exchanges
   * 
   * @returns void
   */
  const fetchExchanges = async () => {
    let exchanges = await CoinsProvider.getTopExchanges();
    setExchanges(exchanges);
  };

  useEffect(() => {
    fetchCoins();
    fetchExchanges();

    const interval = setInterval(() => {
      fetchCoins();
      fetchExchanges();
    }, SECONDS_TO_REFRESH * 1000);

    return () => clearInterval(interval);
  }, []);

  return (
    <>
      <Container className="mt-3 mb-5" fluid>
        <Row className="p-3">
          <Col className="pb-3" sm={12} md={4}>
            <CoinPrice
              coinName="Bitcoin"
              coinId="bitcoin"
              color="primary"
            />
          </Col>
          <Col className="pb-3" sm={12} md={4}>
            <CoinPrice
              coinName="Ethereum"
              coinId="ethereum"
              color="primary"
            />
          </Col>
          <Col className="pb-3" sm={12} md={4}>
            <CoinPrice
              coinName="Litecoin"
              coinId="litecoin"
              color="primary"
            />
          </Col>
        </Row>
      </Container>
      <Container fluid>
        <Row className="p-3">
          <Col sm={12} md={12}>
            <TopCoins coins={coins} />
          </Col>
        </Row>
        <Row className="p-3">
          <Col sm={12} md={12}>
            <TopExchanges exchanges={exchanges} />
          </Col>
        </Row>
      </Container>
      <Container>
        <Row className="pb-3">
          <Col sm={12} md={12}>
            <CoinHistory coins={coins} />
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default Home;