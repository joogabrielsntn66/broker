import React, { useEffect, useMemo, useState } from "react";
import { Button, Col, Container, Row } from "react-bootstrap";
import BrokerTable from "../../components/table/Table";
import CoinsProvider from "../../services/coins";

const Exchanges = (props) => {
  const [exchanges, setExchanges] = useState([]);

  useEffect(() => {
    CoinsProvider.getAllExchanges().then(data => {
      setExchanges(data);
    });
  }, []);
  
  const columns = useMemo(() => [
    {
      Header: "#",
      accessor: "rank",
    },
    {
      Header: "Name",
      accessor: "name", 
    },
    {
      Header: "Volume($)",
      accessor: "volumeUsd",
      Cell: ({ cell: { value } }) => {
        if (!value) {
          return (
            <>
              No Data
            </>
          );
        }

        return (
          <>
            $ {parseFloat(value).toFixed(4).toString()}
          </>
        );
      },
    },
    {
      Header: "Explorer",
      accessor: "exchangeUrl",
      Cell: ({ cell: { value } }) => {
        return (
          <Button variant="primary" href={value}>
            Explorer
          </Button>
        );
      }, 
    },
  ], []);

  return (
    <Container fluid>
      <Row className="pt-4 pb-3">
        <Col xs={12} md={12} sm={12}>
          <BrokerTable
            columns={columns}
            data={exchanges}
            title="Exchanges"
          />
        </Col>
      </Row>
    </Container>
  );
};

export default Exchanges;