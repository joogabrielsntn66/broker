//const API_COINGECKO_URL = 'https://api.coingecko.com/api/v3';
const API_COINCAP_URL = 'https://api.coincap.io/v2';
const LIMIT = 5;

const CoinsProvider = {

  getAllCoins: async () => {
    let res = await fetch(API_COINCAP_URL + '/assets', {
      method: 'GET',
      redirect: 'follow',
    });

    let coins = await res.json();

    return coins.data;
  },

  getAllExchanges: async () => {
    let res = await fetch(API_COINCAP_URL + '/exchanges', {
      method: 'GET',
      redirect: 'follow',
    });

    let coins = await res.json();

    return coins.data;
  },

  getTopCoins: async () => {
    let res = await fetch(API_COINCAP_URL + '/assets?limit=' + LIMIT, {
      method: 'GET',
      redirect: 'follow',
    });

    let coins = await res.json();

    return coins.data;
  },

  getTopExchanges: async () => {
    let res = await fetch(API_COINCAP_URL + '/exchanges?limit=' + LIMIT, {
      method: 'GET',
      redirect: 'follow',
    });

    let coins = await res.json();

    return coins.data;
  },

  getCoinHistory: async (cryptoCurrency, dateInterval) => {
    var dateNow = new Date();
    var dateMonthAgo = new Date();
    dateMonthAgo.setMonth(dateMonthAgo.getMonth() - 1);

    let endPoint = `/assets/${cryptoCurrency}/history?interval=${dateInterval}&start=${dateMonthAgo.getTime()}&end=${dateNow.getTime()}`;

    let res = await fetch(API_COINCAP_URL + endPoint, {
      method: 'GET',
      redirect: 'follow',
    });

    let coinHistory = await res.json();

    let dates = coinHistory.data.map((e) => new Date(e.time).toLocaleDateString('en-US'));
    let prices = coinHistory.data.map((e) => parseFloat(e.priceUsd).toFixed(4));

    return {
      id: cryptoCurrency,
      dates: dates,
      prices: prices,
    };
  },

  getCoinById: async (coinId) => {
    let res = await fetch(API_COINCAP_URL + '/assets/' + coinId, {
      method: 'GET',
      redirect: 'follow',
    });

    let coins = await res.json();

    return coins.data;
  },
};

export default CoinsProvider;